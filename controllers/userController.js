const User = require('../models/User');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// registration
const registerUser = async (req, res) => {
    try {

        let newUser = new User({
            firstName: req.body.firstName,
            lastName: req.body.lastName,
            email: req.body.email,
            password: bcrypt.hashSync(req.body.password, 10),
            mobileNo: req.body.mobileNo,
            address: {
                region: req.body.address.region,
                province: req.body.address.province,
                city: req.body.address.city,
                barangay: req.body.address.barangay,
                street: req.body.address.street
            }
        });


        const result = await newUser.save();
        return res.send(result);


    } catch (error) {
      
       return res.send(error.message)
    }

};


//login user
const loginUser = async (req, res) => {
    try {
        const findUser = await User.findOne({ email: req.body.email });
        if (!findUser) {
            return res.send("User Email is incorrect!!")
        }
        const isPasswordCorrect = await bcrypt.compareSync(req.body.password, findUser.password);
        if (isPasswordCorrect) {
            return res.send({ access: auth.createAccessToken(findUser) })
        } else {
            return res.send("Password does not match!")
        }

    } catch (error) {
        return res.send(error.message)
    }

}


//get user information/details
const userDetails = async (req, res ) => {
	try{
	  	const verifiedUser = await auth.decode(req.headers.authorization);
	  	
        if(verifiedUser.isAdmin){
            const allUsersData = await User.find({_id:{ $ne: verifiedUser.id}});
            return res.send(allUsersData);
	  	}

	  	const data = await User.findById(verifiedUser.id,{password:0,_id: 0,__v:0, isAdmin:0});
        data.password = '********';
	  	return res.send(data);
       


	} catch (error) {
		return res.send(error.message)
	}
};


//update user status 
const userStatus = async (req, res)=> {
    try{
        const verifiedUser = await auth.decode(req.headers.authorization);
        
        if(!verifiedUser.isAdmin){
            return res.send("You are not an Admin!");     
        }

        const statusUpdate = await User.findByIdAndUpdate(req.params.userId,{isAdmin:req.body.isAdmin},{new:true}); 
        return res.send(statusUpdate);

    } catch (error) {
        return res.send(error.message)
    }
};


module.exports = {
	registerUser,
	loginUser,
    userDetails,
    userStatus
}