const User = require('../models/User');
const Product = require('../models/Product');
const bcrypt = require('bcrypt');
const auth = require('../auth');



// add new product for admin
const addProduct = async (req, res) => {
    try {
        const verifiedUser = await auth.decode(req.headers.authorization);
        const findDuplicate = await Product.find({ name: req.body.name });

        if (!verifiedUser.isAdmin) {
            return res.send("You are not an Admin");
        }
        
        let newProduct = new Product({
            name: req.body.name,
            description: req.body.description,
            category: req.body.category,
            price: req.body.price,
            productCount: req.body.productCount
        });

        const result = await newProduct.save();
        return res.send(result);

    } catch (error) {
        return res.send(error.message);
    }
}


 
//get all active products
const getAllProduct = async (req, res) => {
    try {
        const verifiedUser = await auth.decode(req.headers.authorization);
        const show = req.body.show;

        if (verifiedUser.isAdmin) {
            const active = await Product.find({ isActive: true });
            const notActive = await Product.find({ isActive: false });
            if (show === 'all') {
                return res.send( [{ "active": active }, { "notActive": notActive }]);
            }

            //show not active products only
             if (show === false){
                return res.send(notActive);
            }
        }

        const products = await Product.find({ isActive:true});
        return res.send(products);

    } catch (error) {
        return res.send(error.message);
    }
}

//get specific product
const getProduct = async (req, res) => {
    try {
        const product = await Product.findById(req.params.productId);
        return res.send(product);

    } catch (error) {
        return res.send(error.message);
    }
}


//update product
const updateProduct = async (req, res) => {
    try {
        const verifiedUser = await auth.decode(req.headers.authorization);
        if (!verifiedUser.isAdmin) {
            return res.send("You are not an Admin");
        }

        const product = await Product.findByIdAndUpdate(req.params.productId, req.body,{new:true});
        return res.send(product);

    } catch (error) {
        return res.send(error.message);
    }
}


//achive specific product
const archiveProduct = async (req, res) => {
    try {
        const verifiedUser = await auth.decode(req.headers.authorization);
        if (!verifiedUser.isAdmin) {
            return res.send("You are not an Admin");
        }
        const product = await Product.findByIdAndUpdate(req.params.productId, {isActive:false} ,{new:true});
        return res.send({
            productId: product._id,
            productName:product.name,
            status:product.isActive}
            );

    } catch (error) {
       return res.send(error.message);
    }
}
module.exports = {
    addProduct,
    getAllProduct,
    getProduct,
    updateProduct,
    archiveProduct
}