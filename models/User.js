const mongoose = require('mongoose');

const userSchema = new mongoose.Schema({
	firstName: {
		type: String,
		required : [true, "First Name is required"]
	},
	lastName: {
		type: String,
		required : [true, "Last Name is required"]
	},
	email:{
		type: String,
		required : [true, "E-mail is required"],
		unique:true
	},
	password:{
		type: String,
		required : [true, "password is required"]
	},
	isAdmin:{
		type: Boolean,
		default: false
	},
	mobileNo:{
		type: String,
		required: [true,"Mobile number is required"]
	},
	address:{
		region:{
			type:String,
			required: [true,"Region is required"]
		},
		province:{
			type:String,
			required: [true,"Province is required"]
		},
		city:{
			type:String,
			required: [true,"City is required"]
		},
		barangay:{
			type:String,
			required: [true,"Barangay is required"]
		},
		street:{
			type:String,
			required: [true,"Street is required"]
		}

	}
});


module.exports = mongoose.model("User", userSchema);