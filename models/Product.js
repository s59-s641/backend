const mongoose = require('mongoose');

const productSchema = new mongoose.Schema({
    name: {
        type: String,
        required: [true, "Product Name is required"],
        unique:true
    },
    description: {
        type: String,
        required: [true, "Description is required"],
    },
    category: {
        type: [{type:String}],
        validate: {
            validator: arr => arr.length > 0,
            message: () => "Category field is required"
        }
    },
    price: {
        type: Number,
        required: [true, "Price is required"]
    },
    productCount: {
        type: Number,
        required: [true, "Count is required"]
    },
    isActive: {
        type: Boolean,
        default: true
    },
    createdOn: {
        type: Date,
        default: new Date()
    }

});

module.exports = mongoose.model("Product", productSchema);
