const express = require('express');
const router = express.Router();
const auth = require('../auth');
const {
	addProduct,
	getAllProduct,
	getProduct,
	updateProduct,
	archiveProduct
} = require('../controllers/productController');



//create product / get all products
router.route("/").post(auth.verify, addProduct).get(getAllProduct);

//get specific product and update product
router.route("/:productId").get(auth.verify, getProduct).put(auth.verify, updateProduct);


//archive route
router.patch("/:productId/archive", auth.verify, archiveProduct)





module.exports= router;