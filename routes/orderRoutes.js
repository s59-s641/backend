const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	orderProduct,
	orderedProduct
	} = require("../controllers/orderController");

//order product /orderedproducts
router.route("/").post(auth.verify, orderProduct).get(auth.verify, orderedProduct);


module.exports = router
 