
const express = require('express');
const router = express.Router();
const auth = require('../auth');

const {
	registerUser,
	loginUser,
	userDetails,
	userStatus
} = require('../controllers/userController');


// user registration
router.post("/register", registerUser );

// user login
router.post("/login", loginUser);


//get user details  (users and admin)
router.get("/details", auth.verify, userDetails);


//update user status (admin)
router.patch("/details/:userId", auth.verify, userStatus);



module.exports = router;
